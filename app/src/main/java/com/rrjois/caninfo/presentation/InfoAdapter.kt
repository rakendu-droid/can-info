package com.rrjois.caninfo.presentation

import android.content.Context
import android.content.Intent
import com.bumptech.glide.request.target.Target
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.gif.GifDrawable
import com.bumptech.glide.request.RequestListener
import com.rrjois.caninfo.R
import com.rrjois.caninfo.data.Row
import kotlinx.android.synthetic.main.info_item_view.view.*

class InfoAdapter(context: Context): RecyclerView.Adapter<InfoAdapter.canInfoViewHolder>() {

     val context:Context = context
    var infoItems= arrayListOf<Row>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): canInfoViewHolder {
        val view  = LayoutInflater.from(context).inflate(R.layout.info_item_view, parent, false)
        return canInfoViewHolder(view)
    }

    override fun getItemCount(): Int {
            return infoItems.size
    }

    fun clear() {
        infoItems.clear()
        notifyDataSetChanged()
    }


// Add a list of items -- change to type used

    fun addItems(list: List<Row>) {
        infoItems.addAll(list)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: canInfoViewHolder, position: Int) {
        val infoItem = infoItems.get(position)
        if(infoItem!=null && infoItem.imageHref!=null) {
            holder.itemView.title.text = infoItem.title
            holder.itemView.description.text = infoItem.description
            Glide.with(context)
                .load(infoItem.imageHref)
                .dontAnimate()
                .override(Target.SIZE_ORIGINAL)
                .into(holder.itemView.imageView)
            holder.title = infoItem.title
            holder.description = infoItem.description
            holder.imgUrl = infoItem.imageHref

        }

    }


    inner class canInfoViewHolder(view: View,var title:String?="",var description:String?="",var imgUrl:String?=""):RecyclerView.ViewHolder(view){

        init{
            view.setOnClickListener{
                val intent = Intent(context, DetailActivity::class.java)
                intent.putExtra("title", title)
                intent.putExtra("description", description)
                intent.putExtra("imgUrl", imgUrl)
                context.startActivity(intent)
            }
        }
    }
}