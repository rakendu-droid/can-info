package com.rrjois.caninfo.presentation

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.rrjois.caninfo.R
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val title = detail_title
        val description = detail_description
        val imageView = imageView_detail

        title.text = intent.getStringExtra("title")
        description.text = intent.getStringExtra("description")
        imageView.setImageURI(Uri.parse(intent.getStringExtra("imgUrl")))

    }
}
