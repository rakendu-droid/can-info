package com.rrjois.caninfo.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.rrjois.caninfo.viewModel.CanInfoViewModel
import com.rrjois.caninfo.R


class MainActivity : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener {


    lateinit var recyclerView: RecyclerView
    private var adapter = InfoAdapter(this)
    private lateinit var swipeContainer: SwipeRefreshLayout

    private val visibleThreshold = 3
    private var lastVisibleItem: Int = 0
    private var totalItemCount: Int = 0
    private var loading: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView = recycler_view

        swipeContainer = refreshContainer
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        swipeContainer.setOnRefreshListener(this)
        getInfo()
        swipeContainer.setColorSchemeResources(
            android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light
        )

        recyclerView.addItemDecoration(DividerItemDecoration(this))
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager = recyclerView
                    .layoutManager as LinearLayoutManager
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!loading && linearLayoutManager!!.itemCount <= linearLayoutManager.findLastVisibleItemPosition() + visibleThreshold) {
                    loading = true
                    getInfo()
                }
            }
        })
    }

    override fun onRefresh() {
        adapter.clear()
        getInfo()
    }

    private fun getInfo() {
        val viewModel = ViewModelProviders.of(this).get(CanInfoViewModel::class.java)
        viewModel.init()
        viewModel.getNewsRepository()?.observe(this, Observer { infoModel ->
            val infoItems = infoModel.rows
            adapter.addItems(infoItems)
            adapter.notifyDataSetChanged()
            swipeContainer.isRefreshing = false
            loading = false
        })
    }

}
