package com.rrjois.caninfo.data

data class infoModel(
    val rows: List<Row>,
    val title: String
)