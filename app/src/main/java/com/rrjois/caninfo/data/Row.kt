package com.rrjois.caninfo.data

data class Row(
    val description: String,
    val imageHref: String,
    val title: String
)