package com.rrjois.caninfo.HTTP

import androidx.lifecycle.MutableLiveData
import com.rrjois.caninfo.data.infoModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Repository {

    val apiClient: APIClient = RetrofitService.createService()


    companion object{
        val instance = Repository()
    }


    fun getInfo():MutableLiveData<infoModel>{
         var info:MutableLiveData<infoModel> = MutableLiveData()

        apiClient.getCanadaInfo().enqueue(object : Callback<infoModel>{
            override fun onFailure(call: Call<infoModel>, t: Throwable) {
                info.value = null
            }

            override fun onResponse(call: Call<infoModel>, response: Response<infoModel>) {
                if(response.isSuccessful){
                    info.value = response.body()
                }
            }

        })
        return info
    }
}