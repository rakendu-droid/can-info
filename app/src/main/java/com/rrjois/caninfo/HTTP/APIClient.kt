package com.rrjois.caninfo.HTTP

import com.rrjois.caninfo.data.infoModel
import retrofit2.Call
import retrofit2.http.GET

interface APIClient {



    @GET("/s/2iodh4vg0eortkl/facts.json")
    fun getCanadaInfo():Call<infoModel>

}