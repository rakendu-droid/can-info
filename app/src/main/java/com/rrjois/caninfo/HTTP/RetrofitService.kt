package com.rrjois.caninfo.HTTP

import com.rrjois.caninfo.utils.Constants
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitService {

    companion object {
        fun createService(): APIClient {
            return Retrofit.Builder()
                .baseUrl(Constants.BASE_SEARCH_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(APIClient::class.java)
        }
    }
}