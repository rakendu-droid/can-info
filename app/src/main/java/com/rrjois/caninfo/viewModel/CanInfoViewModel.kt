package com.rrjois.caninfo.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.LiveData
import com.rrjois.caninfo.HTTP.Repository
import com.rrjois.caninfo.data.infoModel


class CanInfoViewModel : ViewModel() {

    private var mutableLiveData: MutableLiveData<infoModel>?= null
    private val repository: Repository = Repository.instance

    fun init(){
        mutableLiveData = repository.getInfo()
    }

    fun getNewsRepository(): LiveData<infoModel>? {
        return mutableLiveData
    }

}